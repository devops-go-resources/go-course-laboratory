package main

import "fmt"

func main() {
	exampleCreateSlice()
	exampleIterateWithRange()
	exampleModifyLastValuesUsingLenFunction()
	exampleCopySlice()
}

func exampleCreateSlice() {
	anSlice := []int{1, 2, 3, 4, 5}
	anSlice = append(anSlice, 1)
	anSlice = append(anSlice, 1)

	otherSlice := anSlice[3:5]
	fmt.Println(">", otherSlice, "<")

	fmt.Println("Elementos del slice:")
	for index := 0; index < len(anSlice); index++ {
		fmt.Println(anSlice[index])
	}
}

func exampleIterateWithRange() {
	fruits := []string{"manzana", "banana", "naranja"}

	fmt.Println("Elementos del slice fruits:")
	for _, value := range fruits {
		fmt.Println(value)
	}
}

func exampleModifyLastValuesUsingLenFunction() {
	floatNumbers := []float64{1.5, 2.5, 3.5, 4.5}
	floatNumbers[len(floatNumbers)-1] = 10.5

	fmt.Println("floatNumbers modificado:")
	for _, value := range floatNumbers {
		fmt.Println(value)
	}
}

func exampleCopySlice() {
	slice1 := []int{1, 2, 3, 4, 5}
	slice2 := make([]int, len(slice1))
	copy(slice2, slice1)

	fmt.Println("Nuevo slice copiado:")
	for _, value := range slice1 {
		fmt.Println(value)
	}
}
