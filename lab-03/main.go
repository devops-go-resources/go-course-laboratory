package main

import (
	"fmt"
	"time"
)

func main() {
	// Entero con signo de 8 bits
	var entero8 int8 = -12
	fmt.Println(entero8)

	// Entero sin signo de 16 bits
	var entero16 uint16 = 65535
	fmt.Println(entero16)

	// Flotante de 32 bits
	var flotante32 float32 = 12
	fmt.Println(flotante32)

	// Booleano
	var booleano bool = true
	fmt.Println(booleano)

	// Cadena de caracteres
	var cadena string = "Hola, mundo!"
	fmt.Println(cadena)

	// Runa (carácter Unicode)
	var runa rune = 'a'
	fmt.Println(runa)

	// Array: colección de elementos del mismo tipo con longitud fija
	var arreglo [3]string = [3]string{"1", "2", "3"}
	fmt.Println(arreglo)

	// Slice: segmento variable de un array
	var slice []int = []int{4, 5, 6, 8}
	fmt.Println(slice)

	// Mapa: colección no ordenada de pares clave-valor
	var mapa map[string]int = map[string]int{"a": 1, "b": 2}
	fmt.Println(mapa)

	// Estructura: colección de campos con nombres
	type Persona struct {
		Nombre string
		Edad   int
	}
	persona := Persona{"Juan", 30}
	fmt.Println(persona)

	// Puntero: variable que almacena la dirección de memoria de otra variable
	var puntero *int
	numero := 42
	puntero = &numero
	fmt.Println(*puntero)

	// Canal: mecanismo de comunicación entre goroutines
	canal := make(chan string)
	go func() {
		canal <- "Hola desde el canal!"
	}()
	mensaje := <-canal
	fmt.Println(mensaje)

	// Tipo vacío
	var vacio interface{}
	vacio = 42
	fmt.Println(vacio)

	// Tipo de tiempo
	ahora := time.Now()
	fmt.Println(ahora)

	var age, position int
	age, position = 12, 12
	fmt.Println(age, position)
}
