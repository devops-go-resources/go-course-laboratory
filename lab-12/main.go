package main

import "fmt"

func main() {
	exampleForArray()
	exampleForMap()
	exampleForStruct()
}

func exampleForNumber() {
	fmt.Println("\nEjemplo de bucle for que itera un número determinado de veces:")
	for i := 0; i < 5; i = i + 1 {
		fmt.Printf("Iteración: %d\n", i)
	}
}

func exampleForStruct() {
	type person struct {
		Name string
		Age  int
	}

	people := []person{
		{Name: "Alice", Age: 30},
		{Name: "Bob", Age: 25},
		{Name: "Charlie", Age: 35},
	}

	fmt.Println("\nEjemplo de bucle for que recorre un slice de structs:")
	for _, p := range people {
		fmt.Printf("Nombre: %s, Edad: %d\n", p.Name, p.Age)
	}
}

func exampleForArray() {
	numbers := []string{"haaaaaaa"}

	fmt.Println("Ejemplo de bucle for que recorre un array:")
	for _, value := range numbers {
		fmt.Printf("Valor: %s\n", value)
	}
}

func exampleForMap() {
	mapping := map[string]int{"france": 112, "b": 2, "c": 3}

	fmt.Println("\nEjemplo de bucle for que recorre un mapa:")
	for key, value := range mapping {
		fmt.Printf("Clave: %s, Valor: %d\n", key, value)
	}
}
