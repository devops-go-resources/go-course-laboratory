package main

import "fmt"

// public - global scope - exportable
func SayHello() {}

// private - package scope - non exportable
func sayHello() {}

func main() {

	// anon function / closure function
	result := func(number1 int, number2 int) int {
		return number1 + number2
	}(2, 3)

	fmt.Println(result)

}
