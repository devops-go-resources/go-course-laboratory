package main

// Person representa a una persona con un nombre
type Person struct {
	name string // name es una variable privada, solo accesible dentro del paquete
}

// PersonActions define una interfaz con un método público y uno privado
type PersonActions interface {
	PublicMethod() string
	privateMethod() string // privateMethod es un método privado, solo accesible dentro del paquete
}

// PublicMethod es un método público que implementa PersonActions
func (p Person) PublicMethod() string {
	return "Hello, my name is " + p.name
}

// privateMethod es un método privado que implementa PersonActions
// Solo es accesible dentro del paquete main
func (p Person) privateMethod() string {
	return "This is a private method"
}

// NewPerson crea una nueva instancia de Person
func NewPerson(name string) *Person {
	return &Person{name: name}
}
