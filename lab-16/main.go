// main.go
package main

import "fmt"

func main() {
	// Crear una instancia de Person
	var person *Person = NewPerson("John")

	// Acceder al método público a través de la interfaz
	fmt.Println(person.PublicMethod())

	// Intentar acceder al método privado resultará en un error de compilación
	// fmt.Println(actions.privateMethod()) // Esto no es posible
}
