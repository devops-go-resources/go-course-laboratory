package main

import "fmt"

func main() {
	var name string
	name = "Marco"
	fmt.Println(name)

	street := "Calle Uno"
	fmt.Println(street)

	zipCode := 45340
	fmt.Println(zipCode)

	var uno, dos, tres string = "va", "va", "va"
	fmt.Println(uno, dos, tres)

	var (
		surname string
		age     int
	)
	fmt.Println(surname, age)

}

/*
.. int.........> 64 signed , 32 signed 0
.. bool........> true|false false
.. string......> Caracteres UTF-8, cadena de texto 1 o mas chars ""
.. float64.....> numeros con decimales 0.0
.. slice.......> lista de tamaño dinamico nil
.. map.........> clave/valor, diccionario nil
.. struct......> coleccion de atributos {}
.. interface...> named attr y metodos especificos  nil
.. pointer.....> almacen de direccion de memoria de otra variable, no a la variable
.. channel.....> pipe buffered o no buffered y envia datos asincronos o los recibe
.. uint8.......> 8bits unsigned 0-255
.. uint16......> 16bits unsigned 0-65535
.. uint32......> 32bits unsigned muchos numeros positivos
.. uint64......> 64bits unsigned muchos numeros positivos
.. int8........> 8bits con signo -127,128
.. int16.......> 16bits con signo -32768,32767
.. int32.......> 32bits con signo muchos numeros positivos
.. int64.......> 64bits con signo muchos numeros positivos
.. rune........> char code/unicode 0
.. uint........> unsigned 32 o 64 0
.. complex64
.. complex12
.. uintptr
.. array []
.. function
*/
