FROM golang:1.19-alpine AS builder

ARG LAB_DIR

WORKDIR /app
COPY $LAB_DIR .
COPY lab-packages/ /usr/local/go/src/
RUN apk add --update clang gcc g++ && \
    test -f go.mod || go mod init myapp && \
    test -f go.mod || go mod tidy && \
    go get -v -x github.com/mattn/go-sqlite3 && \
    go build -o main && chmod u+x main

ENV GOBIN /go/bin
ENV GOROOT /usr/local/go/src

CMD ["./main"]