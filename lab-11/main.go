package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Contact struct {
	Email string
}

type Employee struct {
	Person
	Contact
	Salary float64
}

func exampleEmbeddingStructs() {
	employee := Employee{
		Person: Person{
			Name: "Bob",
			Age:  35,
		},
		Contact: Contact{
			Email: "bob@example.com",
		},
		Salary: 50000.0,
	}
	fmt.Println("Name:", employee.Name)
	fmt.Println("Age:", employee.Age)
	fmt.Println("Email:", employee.Email)
	fmt.Println("Salary:", employee.Salary)
}

func main() {
	exampleStructWithFields()
	exampleAnonymousStruct()
}

func exampleAnonymousStruct() {
	person := struct {
		Name string
		Age  int
	}{
		Name: "Alice",
		Age:  25,
	}
	fmt.Println("Name:", person.Name)
	fmt.Println("Age:", person.Age)
}

func exampleStructWithFields() {
	person := Person{
		Name: "John",
		Age:  30,
	}
	fmt.Println("Name:", person.Name)
	fmt.Println("Age:", person.Age)
}
