package main

import (
	"fmt"
)

func main() {
	fmt.Println("")
	basicDefer()
	fmt.Println("")

	fmt.Println("")
	stateDefer()
	fmt.Println("")

	fmt.Println("")
	panicDefer()
	fmt.Println("")
}

func panicDefer() {
	fmt.Println("Starting panicDefer")
	defer fmt.Println("Deferred call in panicDefer")
	fmt.Println("Before panic in panicDefer")
	panic("Something went wrong")
	fmt.Println("After panic in panicDefer") // Esta línea no se ejecutará
}

func stateDefer() {

	fmt.Println("Starting stateDefer")

	originalValue := "original"

	fmt.Println("Original value:", originalValue)
	defer func() {
		originalValue = "original"
		fmt.Println("Restored value:", originalValue)
	}()
	originalValue = "modified"
	fmt.Println("Modified value:", originalValue)
}

func basicDefer() {
	fmt.Println("Starting basicDefer")
	defer fmt.Println("Deferred call in basicDefer")
	fmt.Println("Ending basicDefer")
}
