package main

import "fmt"

func main() {
	ifStatement()
	ifWithInitialization()
	ifWithFunctionCall()
}

func switchStatement() {
	dayOfWeek := "Friday"
	switch dayOfWeek {
	case "Monday":
		fmt.Println("It's Monday")
	case "Tuesday":
		fmt.Println("It's Tuesday")
	case "Wednesday":
		fmt.Println("It's Wednesday")
	case "Thursday":
		fmt.Println("It's Thursday")
	case "Friday":
		fmt.Println("It's Friday")
	default:
		fmt.Println("It's a weekend day")
	}
}

func getTemperature() int {
	return 10
}

func ifWithFunctionCall() {
	if temp := getTemperature(); temp > 0 {
		fmt.Println("The temperature is above zero")
	} else {
		fmt.Println("The temperature is at or below zero")
	}
}

func ifWithInitialization() {
	temperature := 10
	if temp := temperature; temp > 0 {
		fmt.Println("The temperature is above zero")
	} else {
		fmt.Println("The temperature is at or below zero")
	}
}

func ifStatement() {
	number := 10
	if number >= 0 {
		fmt.Println("The number is positive")
	} else if number == 5 {
		fmt.Println("The number is 5")
	} else if number == 5 {
		fmt.Println("The number is 5")
	} else if number == 5 {
		fmt.Println("The number is 5")
	} else if number == 5 {
		fmt.Println("The number is 5")
	} else if number == 5 {
		fmt.Println("The number is 5")
	} else {

	}
}
