package main

import "fmt"

func main() {
	exampleCreateArray()
	exampleIterateArrays()
	exampleModifyArray()
}

func exampleCreateArray() {
	arrayShortWay := [5]int{1, 2, 3, 4, 5}
	fmt.Println(arrayShortWay)

	emptyShortWay := [5]int{0, 0, 0, 0, 0}
	fmt.Println(emptyShortWay)

	var arrayLongWay [5]int = [5]int{1, 2, 3, 4, 5}
	fmt.Println(arrayLongWay)

	var stringCollection [5]string = [5]string{"Hola"}
	fmt.Println(stringCollection)
}

func exampleIterateArrays() {
	numbers := [5]int{1, 2, 3, 4, 5}
	fmt.Println("Elementos del array de numeros:")
	for index := 0; index < len(numbers); index++ {
		fmt.Println(numbers[index])
	}

	names := [3]string{}
	names[0] = "Nombre0"
	names[1] = "Nombre1"
	names[2] = "Nombre2"
	fmt.Println("Elementos del array de cadenas:")
	for index := 0; index < len(names); index++ {
		fmt.Println(names[index])
	}
}

func exampleModifyArray() {
	collection := [4]float64{1.5, 2.5, 3.5, 4.5}

	collection[2] = 10.5

	fmt.Println("Array modificado:")
	for i := 0; i < len(collection); i++ {
		fmt.Println(collection[i])
	}
}
