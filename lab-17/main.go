package main

import (
	"fmt"
)

// Encapsulación: Definimos una estructura con campos privados
type Persona struct {
	nombre string
	edad   int
}

// Método para acceder a los campos privados (getter)
func (p *Persona) GetNombre() string {
	return p.nombre
}

// Método para modificar los campos privados (setter)
func (p *Persona) SetNombre(nombre string) {
	p.nombre = nombre
}

func ejemploEncapsulacion() {
	persona := &Persona{nombre: "Juan", edad: 30}
	fmt.Println("Nombre antes de cambiar:", persona.GetNombre())
	persona.SetNombre("Carlos")
	fmt.Println("Nombre después de cambiar:", persona.GetNombre())
}

// Composición: Definimos estructuras que se componen de otras estructuras
type Empleado struct {
	Persona
	salario float64
}

func ejemploComposicion() {
	empleado := Empleado{Persona: Persona{nombre: "Ana", edad: 25}, salario: 50000}
	fmt.Println("Nombre del empleado:", empleado.GetNombre())
	fmt.Println("Salario del empleado:", empleado.salario)
}

// Polimorfismo e Interfaces: Definimos una interfaz y varias estructuras que la implementan
type Forma interface {
	Area() float64
}

type Cuadrado struct {
	lado float64
}

func (c Cuadrado) Area() float64 {
	return c.lado * c.lado
}

type Circulo struct {
	radio float64
}

func (c Circulo) Area() float64 {
	return 3.14 * c.radio * c.radio
}

// Función que usa una interfaz (ejemplo de principio de inversión de dependencia de SOLID)
func calcularArea(f Forma) {
	fmt.Println("El área es:", f.Area())
}

func ejemploPolimorfismo() {
	cuadrado := Cuadrado{lado: 4}
	circulo := Circulo{radio: 5}

	calcularArea(cuadrado)
	calcularArea(circulo)
}

func main() {
	fmt.Println("Ejemplo de Encapsulación:")
	ejemploEncapsulacion()
	fmt.Println()

	fmt.Println("Ejemplo de Composición:")
	ejemploComposicion()
	fmt.Println()

	fmt.Println("Ejemplo de Polimorfismo:")
	ejemploPolimorfismo()
}
