package main

import "fmt"

func main() {
	var nombre string
	var isAlive bool
	nombre, _, isAlive = nombreFunction()

	fmt.Println(nombre, isAlive)
}

func nombreFunction() (string, int, bool) {
	return "soy la funcion hola", 12, false
}
