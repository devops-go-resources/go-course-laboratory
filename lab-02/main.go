package main

import "fmt"

func main() {
	name := "Marco"
	fmt.Println(name)

	var otherName string
	otherName = "Marco"
	fmt.Println(otherName)

	var aNumber int = 123
	fmt.Println(aNumber)
}
