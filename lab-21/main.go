package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Inicio del programa")

	go concurrentFunction()

	fmt.Println("Función principal sigue ejecutándose...")
	fmt.Println("Fin del programa... esperamos 1 segundo a que termine go routine")

	time.Sleep(1 * time.Second)
}

func concurrentFunction() {
	fmt.Println("Inicio de la función CONCURRENTE")
	fmt.Println("Fin de la función CONCURRENTE")
}
