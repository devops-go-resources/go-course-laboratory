package main

import "fmt"

func main() {
	var result int = CalculateSum(1)
	fmt.Println(result)

	var fullname string
	var age int
	fullname, age = GetNameAndAge("marco", "antonio", "12/12/1900")
	fmt.Println(fullname, age)
}

func CalculateSum(numbers ...int) int {
	var result int = 0

	for _, number := range numbers {
		result += number
	}

	return result
}

func GetNameAndAge(name string, surname string, birthdate string) (fullname string, age int) {
	fullname = name + " " + surname
	age = 123

	return fullname, age
}
