package main

import (
	"errors"
	"fmt"
	"os"
)

func main() {
	fmt.Println("")
	safeFunction()
	fmt.Println("")

	fmt.Println("")
	fileOpenReturnError()
	fmt.Println("")

	fmt.Println("")
	resultOk, notAnError := divide(10, 2)
	fmt.Println("divide(10,2):", resultOk, notAnError)
	resultKo, itsAnError := divide(10, 0)
	fmt.Println("divide(10,0):", resultKo, itsAnError)
	fmt.Println("")
}

func safeFunction() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from panic:", r)
		}
	}()

	fmt.Println("Executing risky function.")
	panic("something went wrong")
}

func fileOpenReturnError() {
	file, err := os.Open("example.txt")
	if err != nil {
		fmt.Println("Error al abrir el archivo:", err)
		return
	}
	defer file.Close()

	fmt.Println("Archivo abierto exitosamente")
}

func divide(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("no se puede dividir por cero")
	}
	return a / b, nil
}
