#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )/"
cd $DIR

LAB_DIR=$1

docker image rm --force go-from-scratch-for-devops-lab >/dev/null 2>&1
docker build --build-arg LAB_DIR="${LAB_DIR}" -t go-from-scratch-for-devops-lab . && docker run --rm go-from-scratch-for-devops-lab && \
docker image rm --force go-from-scratch-for-devops-lab >/dev/null 2>&1