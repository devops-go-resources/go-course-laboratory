package main

import "fmt"

// Ejemplo 1: Pasar un puntero como argumento para modificar el valor original
func modifyValue(x *int) {
    *x = 10
}

func exampleModifyValue() {
    a := 5
    fmt.Println("Antes de modifyValue:", a) // Antes de modifyValue: 5
    modifyValue(&a)
    fmt.Println("Después de modifyValue:", a) // Después de modifyValue: 10
}

// Ejemplo 2: Inicializar un puntero y asignar memoria
func initializePointer() {
    var a *int
    a = new(int)
    *a = 5
    fmt.Println("Valor inicializado:", *a) 
}

// Ejemplo 3: Uso de punteros con estructuras
type Person struct {
    Name string
    Age  int
}

func modifyPerson(p *Person) {
    p.Name = "Alice"
    p.Age = 30
}

func exampleModifyPerson() {
    p := Person{Name: "Bob", Age: 25}
    fmt.Println("Antes de modifyPerson:", p) // Antes de modifyPerson: {Bob 25}
    modifyPerson(&p)
    fmt.Println("Después de modifyPerson:", p) // Después de modifyPerson: {Alice 30}
}

// Ejemplo 4: Punteros a tipos de datos básicos (string)
func modifyString(s *string) {
    *s = "Hello, Go!"
}

func exampleModifyString() {
    str := "Hello, World!"
    fmt.Println("Antes de modifyString:", str) // Antes de modifyString: Hello, World!
    modifyString(&str)
    fmt.Println("Después de modifyString:", str) // Después de modifyString: Hello, Go!
}

// Ejemplo 5: Punteros a arrays
func modifyArray(arr *[3]int) {
    arr[0] = 100
}

func exampleModifyArray() {
    arr := [3]int{1, 2, 3}
    fmt.Println("Antes de modifyArray:", arr) // Antes de modifyArray: [1 2 3]
    modifyArray(&arr)
    fmt.Println("Después de modifyArray:", arr) // Después de modifyArray: [100 2 3]
}

// Ejemplo 6: Punteros a slices (aunque los slices son punteros a arrays internamente)
func modifySlice(slice []int) {
    slice[0] = 200
}

func exampleModifySlice() {
    slice := []int{1, 2, 3}
    fmt.Println("Antes de modifySlice:", slice) // Antes de modifySlice: [1 2 3]
    modifySlice(slice)
    fmt.Println("Después de modifySlice:", slice) // Después de modifySlice: [200 2 3]
}

// Ejemplo 7: Punteros a funciones
func modifyFunc(f *func()) {
    *f = func() {
        fmt.Println("Nueva función")
    }
}

func exampleModifyFunc() {
    f := func() {
        fmt.Println("Función original")
    }
    f() // Función original
    modifyFunc(&f)
    f() // Nueva función
}

func main() {
    exampleModifyValue()
    initializePointer()
    exampleModifyPerson()
    exampleModifyString()
    exampleModifyArray()
    exampleModifySlice()
    exampleModifyFunc()
}
