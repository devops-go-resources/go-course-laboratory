package main

import (
    "fmt"
)

func main(){
    dog := Dog{}
    fmt.Println("Dog speak:", dog.Speak())
    fmt.Println("Dog getName:", dog.GetName())
}

type Animal interface {
    Speak() string
	GetName() string
}

type Dog struct{
}

func (d Dog) Speak() string {
	return "im speak it"
}
func (d Dog) GetName() string {
	return "this is my name"
}
