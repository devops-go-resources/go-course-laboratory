package main

import (
	"fmt"
)

func main() {
	map1 := createMapWithMake()
	map2 := createMapWithLiteral()
	map3 := createEmptyMap()
	map4 := createMapWithNew()

	addElement(map1)
	deleteElement(map2)
	checkKey(map3)
	iterateMap(*map4)
	getLength(map1)
	accessValue(map2)
}

// Example 1: Create a map using make
func createMapWithMake() map[string]int {
	m := make(map[string]int)
	m["a"] = 1
	m["b"] = 2
	return m
}

// Example 2: Create a map using a literal
func createMapWithLiteral() map[string]int {
	m := map[string]int{"c": 3, "d": 4}
	return m
}

// Example 3: Create an empty map
func createEmptyMap() map[string]int {
	var m map[string]int
	return m
}

// Example 4: Create a map using new
func createMapWithNew() *map[string]int {
	m := make(map[string]int)
	(m)["e"] = 5
	return &m
}

// Example 5: Add elements to a map
func addElement(m map[string]int) {
	m["f"] = 6
	fmt.Println("Map after adding element:", m)
}

// Example 6: Delete elements from a map
func deleteElement(m map[string]int) {
	delete(m, "c")
	fmt.Println("Map after deleting element:", m)
}

// Example 7: Check if a key exists in a map
func checkKey(m map[string]int) {
	_, exists := m["a"]
	fmt.Println("Does key 'a' exist in the map?", exists)
}

// Example 8: Iterate over a map
func iterateMap(m map[string]int) {
	for key, value := range m {
		fmt.Printf("Key: %s, Value: %d\n", key, value)
	}
}

// Example 9: Get the length of a map
func getLength(m map[string]int) {
	fmt.Println("Length of the map:", len(m))
}

// Example 10: Access a specific value in a map
func accessValue(m map[string]int) {
	value := m["d"]
	fmt.Println("Value corresponding to key 'd':", value)
}
