package main

import (
	"fmt"
)

func main() {

	fmt.Println("")
	lanzaPanic()
	fmt.Println("")

	fmt.Println("")
	fmt.Println("AQUI NO VA A LLEGAR NUNCA")
	fmt.Println("")
}

func lanzaPanic() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recuperado del pánico en ejemploBasico:", r)
		}
	}()

	fmt.Println("Antes del pánico en ejemploBasico")
	panic("algo salió mal en ejemploBasico")
}
